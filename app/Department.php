<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    public function company(){
        return $this->belongsTo(Company::class);
    }

    public function employees(){
        return$this->hasMany(User::class, 'user_id');
    }
}
