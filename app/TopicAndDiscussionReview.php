<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TopicAndDiscussionReview extends Model
{
    public $table = "topic_and_discussion_reviews";

    public $fillable = ['created_at', 'message', 'topic_and_discussions_id', 'reaction', 'rate'];

    public function employee(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function topic_and_discussion(){
        return $this->belongsTo(User::class, 'topic_and_discussions_id');
    }
}
