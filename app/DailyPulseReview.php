<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class DailyPulseReview extends Model
{
    public $table = "daily_pulse_reviews";

    public $fillable = ['user_id', 'in_general', 'my_performance', 'teams_performance', 'team_progress', 'team_atmosphere', 'dinner_quality', 'office_cleanliness', 'management_decisions', 'created_at'];

    public function employee(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function toArray(){
        return [
            'in_general' => $this->in_general,
            'my_performance' => $this->my_performance,
            'teams_performance' => $this->teams_performance,
            'team_progress' => $this->team_progress,
            'team_atmosphere' => $this->team_atmosphere,
            'dinner_quality' => $this->dinner_quality,
            'office_cleanliness' => $this->office_cleanliness,
            'management_decisions' => $this->management_decisions,
        ];
    }
}
