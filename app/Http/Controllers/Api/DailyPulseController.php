<?php

namespace App\Http\Controllers\Api;

use App\DailyPulseReview;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreDailyPulseRequest;
use Carbon\Carbon;

class DailyPulseController extends Controller
{
    public function rate(StoreDailyPulseRequest $request, DailyPulseReview $dailyPulseReview){
        $dailyPulseReview->fill($request->all());
        $dailyPulseReview->user_id = auth('api')->user()->id;
        if ($dailyPulseReview) {
            $dailyPulseReview->save();

            return response()->json('success');
        }


        return response()->json('Something went wrong', 500);

//        $type = request()->type;
//        if($type === 'in_general'){
//            $dailyPulseReview->$type = in_array(request()->rate, ['true', 'True', '1', 1])? 1:0;
//        }else{
//            $dailyPulseReview->$type = request()->rate;
//        }
//
//        $dailyPulseReview->save();

//        return response()->json(optional($dailyPulseReview)->toArray());
    }


    public function show(){
        $dailyPulseReview = DailyPulseReview::whereDate('created_at', Carbon::today())->first();
        if ($dailyPulseReview) {
            return response()->json($dailyPulseReview);
        } else {
            return response()->json('This is the first record today');
        }
//        $dailyPulseReview = auth('api')->user()->daily_pulse_review()->firstOrNew([
//            'created_at' => Carbon::today()
//        ]);
//        return response()->json(optional($dailyPulseReview)->toArray());
    }
}
