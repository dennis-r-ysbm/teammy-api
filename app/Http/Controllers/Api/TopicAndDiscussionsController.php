<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\TopicAndDiscussion;
use App\TopicAndDiscussionReview;
use Illuminate\Database\Eloquent\Builder;

class TopicAndDiscussionsController extends Controller
{
    public function check(){
        $user = auth('api')->user();
        $topicAndDiscussions =
            TopicAndDiscussion::where('company_id', $user->company->id)
                ->whereDoesntHave('topic_and_discussion_reviews', function (Builder $query) use ($user){
                    $query->where('user_id', $user->id);
            })->get();
        if ($topicAndDiscussions) {
            return response()->json($topicAndDiscussions);
        } else {
            return response()->json('Something went wrong', 500);
        }


    }

    public function reaction(TopicAndDiscussion $topicAndDiscussion){
        $type = $topicAndDiscussion->type;

        $topicAndDiscussionReview = new TopicAndDiscussionReview();
        $topicAndDiscussionReview->message = request()->message;
        $topicAndDiscussionReview->topic_and_discussions_id = $topicAndDiscussion->id;
        $topicAndDiscussionReview->user_id = auth('api')->user()->id;
        if($type === 'reaction'){
            $topicAndDiscussionReview->$type = in_array(request()->rate, ['true', 'True', '1', 1])? 1:0;
        }else{
            $topicAndDiscussionReview->$type = request()->rate;
        }
        $topicAndDiscussionReview->save();

        if($topicAndDiscussionReview){
            return response()->json('', 201);
        }

        return response()->json('Something went wrong', 500);
    }

}
