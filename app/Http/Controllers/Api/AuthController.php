<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Invite;
use App\User;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(){
        $credentials = request(['email', 'password']);

        if (! $token = auth('api')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }



        return response()->json([
            'token' => $token,
            'name' => auth('api')->user()->name,
            'department' => optional(auth('api')->user()->department)->name
        ]);
    }

    public function register(){
        $invitation = Invite::where('email', request()->email)->first();
        if($invitation){
            $user = User::create([
                'email'    => request()->email,
                'password' => Hash::make(request()->password),
                'name' => request()->name,
                'company_id' => $invitation->company_id,
                'department_id' => $invitation->department_id,
            ]);

            $token = auth('api')->login($user);
            return response()->json(['token' => $token]);
        }

        return response()->json(
            ['error' => 'The email did not found in the invited list. Contact with your company, please.'],
            401
        );
    }
}
