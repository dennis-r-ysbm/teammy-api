<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreDailyPulseRequest;
use App\Http\Requests\StoreTeamResponseRequest;

class TeamResponsesController extends Controller
{
    public function __invoke(StoreTeamResponseRequest $request)
    {
        $teamResponse = auth('api')->user()->team_responses()->create([
            'file_path' => $request->file('file') ? $request->file('file')->store('team-responses') : null,
            'type' => $request->type,
            'urgency' => $request->urgency,
            'message' => $request->message
        ]);

        if ($teamResponse) {
            return response()->json('', 201);
        }

        return response()->json('Something went wrong', 500);
    }
}
