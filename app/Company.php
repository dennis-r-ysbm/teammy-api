<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    public function topic_and_discussions(){
        return $this->hasMany(TopicAndDiscussion::class);
    }
}
