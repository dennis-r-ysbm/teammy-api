<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TeamResponse extends Model
{
    public $table = "team_responses";

    public $fillable = ['created_at', 'file_path', 'message', 'type', 'urgency'];

    public function employee(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
