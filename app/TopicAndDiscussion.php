<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TopicAndDiscussion extends Model
{
    public $table = "topic_and_discussions";

    public $fillable = ['created_at', 'message', 'type'];

    public function company(){
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function topic_and_discussion_reviews(){
        return $this->hasMany(TopicAndDiscussionReview::class, 'topic_and_discussions_id');
    }
}
