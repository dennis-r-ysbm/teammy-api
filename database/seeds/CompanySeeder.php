<?php

use App\Company;
use App\Department;
use App\Invite;
use App\TopicAndDiscussion;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company = new Company();
        $company->name = "YSBM Group";
        $company->save();

        $department = new Department();
        $department->name  = 'HR';
        $department->company_id = $company->id;
        $department->save();

        $user = new User();
        $user->name = "Admin";
        $user->email = 'admin@gmail.com';
        $user->role = 'admin';
        $user->password = Hash::make('secret');
        $user->company_id = $company->id;
        $user->save();

        $invite = new Invite();
        $invite->email = 'serhii.h.ysbm@gmail.com';
        $invite->company_id = $company->id;
        $invite->department_id = $department->id;
        $invite->save();

        $topicAndDiscussion = new TopicAndDiscussion();
        $topicAndDiscussion->company_id = $company->id;
        $topicAndDiscussion->type = 'reaction';
        $topicAndDiscussion->message = 'Reduce working hours from 8 to 6 hrs';
        $topicAndDiscussion->save();

        $topicAndDiscussion = new TopicAndDiscussion();
        $topicAndDiscussion->company_id = $company->id;
        $topicAndDiscussion->type = 'rate';
        $topicAndDiscussion->message = 'Add vegan meal option from next month';
        $topicAndDiscussion->save();
    }
}
