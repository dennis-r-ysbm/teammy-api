<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDailyPulseReviews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_pulse_reviews', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('in_general')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->smallInteger('my_performance')->nullable();
            $table->smallInteger('teams_performance')->nullable();
            $table->smallInteger('team_atmosphere')->nullable();
            $table->smallInteger('dinner_quality')->nullable();
            $table->smallInteger('office_cleanliness')->nullable();
            $table->smallInteger('management_decisions')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_pulse_reviews');
    }
}
