<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUrgencyAndTypeColumnsToTeamResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('team_responses', function (Blueprint $table) {
            $table->tinyInteger('urgency')->after('message');
            $table->string('type')->after('message');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('team_responses', function (Blueprint $table) {
            $table->dropColumn('urgency');
            $table->dropColumn('type');
        });
    }
}
