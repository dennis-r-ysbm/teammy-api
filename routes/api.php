<?php

Route::group(['namespace'=>'Api'],function() {
    Route::post('login', 'AuthController@login')->name('api.login');
    Route::post('register', 'AuthController@register')->name('api.register');

    Route::group(['middleware' => 'auth:api'],function() {
        Route::group(['prefix'=>'daily-pulse'],function() {
            Route::post('/', 'DailyPulseController@rate')->name('api.daily-pulse.rate');
            Route::get('/', 'DailyPulseController@show')->name('api.daily-pulse.show');
        });

        Route::group(['prefix'=>'team-response'],function() {
            Route::post('/', 'TeamResponsesController')->name('api.team-response');
        });

        Route::group(['prefix'=>'topic-and-discussions'],function() {
            Route::get('/', 'TopicAndDiscussionsController@check')->name('api.topic-and-discussions.check');
            Route::post('/{topicAndDiscussion}', 'TopicAndDiscussionsController@reaction')->name('api.topic-and-discussions.reaction');
        });
    });

});

